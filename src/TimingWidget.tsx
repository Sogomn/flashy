import React from "react";
import FlashTimer from "./FlashTimer";
import "./TimingWidget.css";
import ToggleImage from "./ToggleImage";

const FLASH_ICON_PATH = "images/flash.webp";
const COSMIC_INSIGHT_ICON_PATH = "images/cosmic_insight.png";
const IONIAN_BOOTS_ICON_PATH = "images/ionian_boots.png";

type Props = {
	laneIconPath: string;
	size: number;
	aram: boolean;
};

export default function TimingWidget(props: Props) {
	const [cosmicInsight, setCosmicInsight] = React.useState(false);
	const [ionianBoots, setIonianBoots] = React.useState(false);
	
	return <div className="TimingWidget">
		<img
			src={props.laneIconPath}
			alt=""
			width={props.size}
			height={props.size}
		/>
		<ToggleImage
			iconPath={COSMIC_INSIGHT_ICON_PATH}
			size={props.size}
			callback={setCosmicInsight}
		/>
		<ToggleImage
			iconPath={IONIAN_BOOTS_ICON_PATH}
			size={props.size}
			callback={setIonianBoots}
		/>
		<FlashTimer
			cosmicInsight={cosmicInsight}
			ionianBoots={ionianBoots}
			flashIconPath={FLASH_ICON_PATH}
			size={props.size}
			aram={props.aram}
		/>
	</div>
}
