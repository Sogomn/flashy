import React from "react";
import "./App.css";
import "./FlashTimer.css";
import TimingWidget from "./TimingWidget";
import ToggleImage from "./ToggleImage";

const TIMER_ICON_SIZE = 100;
const ARAM_ICON_SIZE = 50;
const ARAM_ICON_PATH = "images/aram.svg";

function App() {
	const [aram, setAram] = React.useState(false);
	
	return (
		<div className="App">
			<main>
				<TimingWidget
					laneIconPath="images/top.svg"
					size={TIMER_ICON_SIZE}
					aram={aram}
				/>
				<TimingWidget
					laneIconPath="images/jungle.svg"
					size={TIMER_ICON_SIZE}
					aram={aram}
				/>
				<TimingWidget
					laneIconPath="images/mid.svg"
					size={TIMER_ICON_SIZE}
					aram={aram}
				/>
				<TimingWidget
					laneIconPath="images/bot.svg"
					size={TIMER_ICON_SIZE}
					aram={aram}
				/>
				<TimingWidget
					laneIconPath="images/support.svg"
					size={TIMER_ICON_SIZE}
					aram={aram}
				/>
			</main>
			<footer>
				<ToggleImage
					iconPath={ARAM_ICON_PATH}
					size={ARAM_ICON_SIZE}
					callback={setAram}
				/>
			</footer>
		</div>
	);
}

export default App;
