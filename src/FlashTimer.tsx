import React from "react";

const COOLDOWN = 5 * 60;
const INACCURACY_SECONDS = 10;
const TIMER_ADJUSTMENT_AMOUNT_SECONDS = 10;

const COSMIC_INSIGHT_SUMMONER_SPELL_HASTE = 18;
const IONIAN_BOOTS_SUMMONER_SPELL_HASTE = 10;
const ARAM_SUMMONER_SPELL_HASTE = 70;

type Props = {
	flashIconPath: string;
	size: number;
	cosmicInsight: boolean;
	ionianBoots: boolean;
	aram: boolean;
};

export default function FlashTimer(props: Props) {
	const [timer, setTimer] = React.useState(0);
	const [clockHandle, setClockHandle] = React.useState<number | null>(null);
	
	const calculateCooldown = React.useCallback(() => {
		const haste = COSMIC_INSIGHT_SUMMONER_SPELL_HASTE * Number(props.cosmicInsight)
			+ IONIAN_BOOTS_SUMMONER_SPELL_HASTE * Number(props.ionianBoots)
			+ ARAM_SUMMONER_SPELL_HASTE * Number(props.aram);
		
		return COOLDOWN * (100 / (100 + haste));
	}, [props.cosmicInsight, props.ionianBoots, props.aram]);
	
	const countUp = () => {
		setTimer(timer => timer + 1);
	};
	
	const startTimer = () => {
		resetTimer();
		
		const newClockHandle = window.setInterval(countUp, 1000);
		
		setTimer(oldTimer => oldTimer + INACCURACY_SECONDS);
		setClockHandle(newClockHandle);
	};
	
	const resetTimer = React.useCallback(() => {
		if (clockHandle != null) {
			window.clearInterval(clockHandle);
		}
		
		setTimer(0);
		setClockHandle(null);
	}, [clockHandle, setTimer, setClockHandle]);
	
	const increaseTimer = React.useCallback((amount: number) => {
		setTimer(oldTimer => Math.max(oldTimer - amount, 0));
	}, [setTimer]);
	
	const decreaseTimer = React.useCallback((amount: number) => {
		setTimer(oldTimer => oldTimer + amount);
	}, [setTimer]);
	
	React.useEffect(() => {
		const secondsLeft = calculateCooldown() - timer;
		
		if (secondsLeft <= 0) {
			resetTimer();
		}
	}, [timer, calculateCooldown, resetTimer]);
	
	const cooldown = calculateCooldown();
	const secondsLeft = Math.max(Math.floor(cooldown - timer), 0);
	const minutesLeft = String(Math.floor(secondsLeft / 60)).padStart(2, "0");
	const subsecondsLeft = String(secondsLeft % 60).padStart(2, "0");
	const timerString = clockHandle ? `${minutesLeft}:${subsecondsLeft}` : "-";
	const percentageDone = 100 - (secondsLeft / cooldown) * 100;
	const overlayConicStyle = {
		background: `conic-gradient(rgba(0, 0, 0, 0.0) ${percentageDone}%, rgba(0, 0, 0, 0.75) ${percentageDone}%)`,
	};
	
	return (
		<div className="FlashTimer">
			<div className="flash-wrapper">
				<div
					className="flash-overlay"
					style={clockHandle ? overlayConicStyle : undefined}
				/>
				<img
					className="clickable"
					src={props.flashIconPath}
					width={props.size}
					height={props.size}
					alt=""
					onClick={e => {
						if (e.ctrlKey) {
							increaseTimer(TIMER_ADJUSTMENT_AMOUNT_SECONDS);
						} else {
							startTimer();
						}
					}}
					onContextMenu={e => {
						if (e.ctrlKey) {
							decreaseTimer(TIMER_ADJUSTMENT_AMOUNT_SECONDS);
						} else {
							resetTimer();
						}
					}}
				/>
			</div>
			<div className="timer">
				{timerString}
			</div>
		</div>
	);
}
