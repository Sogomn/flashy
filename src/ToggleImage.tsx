import React from "react";

type Props = {
	iconPath: string;
	size: number;
	callback?: (state: boolean) => void;
};

export default function ToggleImage(props: Props) {
	const [state, setState] = React.useState(false);
	
	React.useEffect(() => {
		props.callback?.(state);
	}, [state, props]);
	
	let className = "clickable";
	
	if (!state) {
		className += " disabled";
	}
	
	return <img
		className={className}
		src={props.iconPath}
		width={props.size}
		height={props.size}
		alt=""
		onClick={() => setState(state => !state)}
	/>;
}
